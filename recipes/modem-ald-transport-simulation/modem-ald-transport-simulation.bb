inherit autotools pkgconfig gettext systemd

SUMMARY = "modem_ald_transport_simulation"
SECTION = "qti-modem-ald-transport-simulation"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS += "glib-2.0"

FILESPATH =+ "${WORKSPACE}:"

SRC_URI = "git://github.com/bang-olufsen/yahdlc.git;"
SRCREV = "0c827b7f4266d8a106266072e6bf99e02fddcbad"

SRC_URI += "file://0001-modem-ald-transport-simulation-changes.patch"

S = "${WORKDIR}/git"

CPPFLAGS += "-I${STAGING_INCDIR}"

INSANE_SKIP:${PN} = "dev-so"

PACKAGES = "${PN} ${PN}-dbg"
PACKAGE_ARCH ?= "${MACHINE_ARCH}"

FILES_${PN} = "${libdir}/*"
FILES_${PN} += "${bindir}/*"
FILES_${PN} += "${includedir}/*"
FILES_${PN} += "${sysconfdir}/*"
FILES_${PN}-dev = "${includedir}/*"
FILES_${PN} += "${systemd_unitdir}/system/"
FILES_${PN}-staticdev = "${libdir}/*.a"
FILES_${PN} += "${libdir}/*.so"

do_install_append(){
    install -d ${D}${sysconfdir}
    install -m 0755 ${S}/ald/cfg/* ${D}${sysconfdir}/
}
