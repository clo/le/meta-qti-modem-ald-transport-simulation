SUMMARY = "Modem-ALD-Transport-Simulation package groups"
PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

PROVIDES = "${PACKAGES}"

PACKAGES = ' \
     packagegroup-modem-ald-transport-simulation \
'

RDEPENDS_packagegroup-modem-ald-transport-simulation = ' \
    modem-ald-transport-simulation \
'
